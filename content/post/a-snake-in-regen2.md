+++
title = "a snake in regen2"
date = 2009-01-30T07:01:00Z
updated = 2009-01-30T07:06:50Z
tags = ["Regen2", "python", "packages"]
blogimport = true 
type = "post"
+++

I'm sure you're thinking I'm talking about me ;) nope first order of business, add more stuff which should be in gentoo.<br /><br />I'm pleased to announce that I've added py3k also know as Python 3.0 to Regen2. It will remain hardmasked as unmasking could create stability issues on a gentoo based system given it's reliance on python.<div class="blogger-post-footer"><br />--<br />
This <span xmlns:dc="http://purl.org/dc/elements/1.1/" href="http://purl.org/dc/dcmitype/Text" rel="dc:type">work</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="http://www.xenoterracide.com" property="cc:attributionName" rel="cc:attributionURL">Caleb Cushing</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/">Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License</a>.</div>
