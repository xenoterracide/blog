+++
title = "Programming Language Maturity Model"
date = 2015-08-20T11:22:00Z
updated = 2015-08-20T11:22:08Z
draft = true
blogimport = true 
type = "post"
+++

<p>This is an attempt to apply a <a href="http://martinfowler.com/bliki/MaturityModel.html">Maturity Model</a> to Programming Languages. This model attempts to be paradigm agnostic, there may be other models which could be used to determine how Functional a language that supports functional programming is for example. <dl>  <dt>Level 0</dt>  <dd>Conditional Execution and Variables</dd>  <dt>Level 1</dt>  <dd>Turing Completeness, Iteration and Named Procedures</dd>  <dt>Level 2</dt>  <dd>Module Encapsulation</dd>  <dt>Level 3</dt>  <dd>Library Formats and Build tools</dd>  <dt>Level 4</dt>  <dd>Library Repositories</dd>  <dt>Level 5</dt>  <dd>Standard Libraries</dd></dl></p> <h2>Level 0</h2>
