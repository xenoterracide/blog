+++
title = "accept input from stdin"
date = 2008-10-30T02:11:00Z
updated = 2008-10-30T02:12:50Z
tags = ["emerge-ng", "pms"]
blogimport = true 
type = "post"
+++

if possible emerge-ng should be able to accept input from <STDIN>.<br /><br />e.g. cat packagelist | emergeng<br /><br />thus far neither portage nore pkgcore have this capability.<div class="blogger-post-footer"><br />--<br />
This <span xmlns:dc="http://purl.org/dc/elements/1.1/" href="http://purl.org/dc/dcmitype/Text" rel="dc:type">work</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="http://www.xenoterracide.com" property="cc:attributionName" rel="cc:attributionURL">Caleb Cushing</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/">Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License</a>.</div>
